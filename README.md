
## The Road So Far
- [X] Classes
- [x] Arrays
- [x] Dictionaries
- [X] Classes
- [x] Structs
- [x] Optionals
- [x] Internet
- [x] Core Data
- [x] Gesture Recognizers
- [x] Animations
- [x] Protocols & Delegates
- [x] Closures
- [x] Singletons
- [x] MVC
- [x] Testing
- [x] Firebase


## Technologies
- [ ] AVFoundation 
- [ ] CoreLocation
- [ ] CoreMotion
- [ ] HealthKit
- [ ] CoreImage & CoreGraphics
- [ ] CoreAnimation
- [ ] Haptic Feedback
- [ ] MapKit & 3D Touch
- [ ] CoreBluettoh
- [ ] AudioKit
- [ ] MultipeerConnectivity
- [ ] CloudKit


## The Road to come


- [ ] Generics
- [ ] Notification Center
- [ ] Multithreading
- [ ] Protocol Oriented Programming
- [ ] Functional Programming
- [ ] MVVM
- [ ] Viper
- [ ] RxSwift
- [ ] Realm
- [ ] Objective C Syntax
- [ ] TVOS & WatchOS
- [ ] App Extension - Today & Keyboard
- [ ] Push Notificatons
- [ ] Testing 
- [ ] Server Side Swift - Vapor, Kitura or Perfect
- [ ] MacOS 
- [ ] SpriteKit & SceneKit
- [ ] In App Purchases




## Resources

- [RayWenderlich](https://www.raywenderlich.com)
- [AppCoda](http://appcoda.com/)
- [ObjC.io](https://www.objc.io/)


## Books

*In no particular order*

 - [ProSwift](https://gumroad.com/l/proswift)
 - [Advanced Swift](https://www.objc.io/books/advanced-swift/)
 - [RxSwift](https://store.raywenderlich.com/products/rxswift)
 - [Functional Swift](https://www.objc.io/books/functional-swift/)
